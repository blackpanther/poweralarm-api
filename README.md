# PowerAlarm API

The library/package implements the API provided by PowerAlarm to create alarms and communicate with the app/platform.

Sources:
- Application: [poweralarm.de](https://poweralarm.de/)
- API (PDF): [Section 9: JSON-Schnittstelle](http://www.fitt-gmbh.de/fileadmin/fittcom/pdf/poweralarm/hilfe_poweralarm.pdf)

---

**LICENSE:** [MIT](LICENSE.txt)
