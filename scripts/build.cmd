@echo off
set Configuration=Release

cd "%~dp0"
cd ".."
powershell write-host -fore Blue Restoring solution

rmdir /S /Q artifacts
mkdir artifacts

dotnet restore -v q
:: dotnet tool restore -v q

cd "%~dp0"
cd "..\AMWD.Net.Api.PowerAlarm"
powershell write-host -fore Blue Building PowerAlarm API

rmdir /S /Q bin
dotnet build -c %Configuration% --nologo --no-incremental
move bin\%Configuration%\*.nupkg ..\artifacts
move bin\%Configuration%\*.snupkg ..\artifacts
