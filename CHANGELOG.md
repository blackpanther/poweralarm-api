# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://git.am-wd.de/andreasmueller/poweralarm-api/compare/v1.1.0...main) - 0000-00-00
_no changes yet_


## [v1.1.0](https://git.am-wd.de/andreasmueller/poweralarm-api/compare/v1.0.1...v1.1.0) - 2022-07-16
### Changed
- Own Namespace for Exceptions: `AMWD.Net.Api.PowerAlarm.Exceptions`

### Removed
- External `HttpClient` no longer used


## [v1.0.1](https://git.am-wd.de/andreasmueller/poweralarm-api/compare/v1.0.0...v1.0.1) - 2022-01-10
### Added
- Changelog

### Fixed
- The `Message` for group and contact alarm can now be empty


## [v1.0.0](https://git.am-wd.de/andreasmueller/poweralarm-api/commits/v1.0.0) - 2022-01-09

Initial release
