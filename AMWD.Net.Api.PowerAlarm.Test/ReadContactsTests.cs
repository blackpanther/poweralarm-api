using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using AMWD.Net.Api.PowerAlarm.Responses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class ReadContactsTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new ReadContactsResponse());
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			using var api = GetApi(apikey);

			// act
			var response = await api.ReadContacts();
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("getcontacts", callbackJson["action"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string apikey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.NotFound;

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.ReadContacts();
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual($"HTTP Request error: {httpResponseMessage.StatusCode}", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnErrorResponse()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new ReadContactsResponse { HasError = true, ErrorMessage = "You shall not pass" };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.ReadContacts();
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual(createResponse.ErrorMessage, ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
