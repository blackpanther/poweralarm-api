using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using AMWD.Net.Api.PowerAlarm.Models;
using AMWD.Net.Api.PowerAlarm.Requests;
using AMWD.Net.Api.PowerAlarm.Responses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class CreateGroupAlarmTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new CreateGroupAlarmResponse { IsSuccess = true, Alarm = new GroupAlarmStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new CreateGroupAlarmRequest
			{
				GroupId = "GR01",
				Message = "Achtung Probealarm",
				Latitude = 49.12345f,
				Longitude = 11.12345f,
				Country = "D",
				Street = "Dorfstr.",
				HouseNo = "15b",
				ZipCode = "89001",
				City = "Musterstadt",
				AdditionalInfo = "2. OG",
				SceneOject = "Haus",
				Priority = "Hoch",
				Units = "HLF",
				Tetra = "7654321",
				Fme = "22853"
			};

			using var api = GetApi(apikey);

			// act
			var response = await api.CreateGroupAlarm(request);
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("triggergroupalarm", callbackJson["action"]);
			Assert.AreEqual(request.GroupId, callbackJson["kuerzel"]);
			Assert.AreEqual(request.Message, callbackJson["text"]);
			Assert.AreEqual(request.Latitude?.ToString(), callbackJson["lat"]?.ToString());
			Assert.AreEqual(request.Longitude?.ToString(), callbackJson["long"]?.ToString());
			Assert.AreEqual(request.Country, callbackJson["land"]);
			Assert.AreEqual(request.Street, callbackJson["strasse"]);
			Assert.AreEqual(request.HouseNo, callbackJson["hsnr"]);
			Assert.AreEqual(request.ZipCode, callbackJson["plz"]);
			Assert.AreEqual(request.City, callbackJson["ort"]);
			Assert.AreEqual(request.AdditionalInfo, callbackJson["zusatz"]);
			Assert.AreEqual(request.SceneOject, callbackJson["objekt"]);
			Assert.AreEqual(request.Priority, callbackJson["prioritaet"]);
			Assert.AreEqual(request.Units, callbackJson["einsatzmittel"]);
			Assert.AreEqual(request.Tetra, callbackJson["tetra"]);
			Assert.AreEqual(request.Fme, callbackJson["schleifen"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingParameters()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new CreateGroupAlarmResponse { IsSuccess = true, Alarm = new GroupAlarmStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new CreateGroupAlarmRequest();

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateGroupAlarm(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());

			try
			{
				var response = await api.CreateGroupAlarm(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string apikey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.NotFound;

			var request = new CreateGroupAlarmRequest
			{
				GroupId = "GR01",
				Message = "Achtung Probealarm"
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateGroupAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual($"HTTP Request error: {httpResponseMessage.StatusCode}", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnErrorResponse()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateGroupAlarmResponse { HasError = true, ErrorMessage = "You shall not pass" };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateGroupAlarmRequest
			{
				GroupId = "GR01",
				Message = "Achtung Probealarm"
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateGroupAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual(createResponse.ErrorMessage, ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse1()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateGroupAlarmResponse { IsSuccess = false, Alarm = new GroupAlarmStatus { IsSuccess = true } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateGroupAlarmRequest
			{
				GroupId = "GR01",
				Message = "Achtung Probealarm"
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateGroupAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse2()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateGroupAlarmResponse { IsSuccess = true, Alarm = new GroupAlarmStatus { IsSuccess = false } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateGroupAlarmRequest
			{
				GroupId = "GR01",
				Message = "Achtung Probealarm"
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateGroupAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
