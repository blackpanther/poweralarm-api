using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Enums;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using AMWD.Net.Api.PowerAlarm.Models;
using AMWD.Net.Api.PowerAlarm.Requests;
using AMWD.Net.Api.PowerAlarm.Responses;
using AMWD.Net.Api.PowerAlarm.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class CreateContactsAlarmTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new CreateContactsAlarmResponse { IsSuccess = true, Alarm = new CreateContactsAlarmObject { IsSuccess = true, } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new CreateContactsAlarmRequest
			{
				Message = "Achtung Probealarm",
				IsFeedbackAllowed = true,
				IsInfoMessage = false,
				Contacts = new List<Contact>
				{
					new Contact
					{
						Name = "Kontakt 1",
						Number = "+49171000001",
						Type = AlarmContactMessageType.Voice
					},
					new Contact
					{
						Name = "Kontakt 2",
						Number = "+49171000002",
						Type = AlarmContactMessageType.Text
					},
					new Contact
					{
						Name = "Kontakt 3",
						Number = "+49171000003",
						Type = AlarmContactMessageType.Flash
					},
					new Contact
					{
						Name = "Kontakt 4",
						Number = "+49171000004",
						Type = AlarmContactMessageType.Fax
					}
				}
			};

			using var api = GetApi(apikey);

			// act
			var response = await api.CreateContactsAlarm(request);
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("triggeralarm", callbackJson["action"]);
			Assert.AreEqual(request.Message, callbackJson["text"]);
			Assert.AreEqual(request.IsFeedbackAllowed, callbackJson["feedback"]);
			Assert.AreEqual(request.IsInfoMessage, callbackJson["infomessage"]);

			Assert.AreEqual(request.Contacts.Skip(0).First().Name, callbackJson["destinations"][0]["name"]);
			Assert.AreEqual(request.Contacts.Skip(0).First().Number, callbackJson["destinations"][0]["dnr"]);
			Assert.AreEqual(request.Contacts.Skip(0).First().Type, AlarmContactMessageTypeMapper.GetType(callbackJson["destinations"][0]["msgtype"].ToString()));

			Assert.AreEqual(request.Contacts.Skip(1).First().Name, callbackJson["destinations"][1]["name"]);
			Assert.AreEqual(request.Contacts.Skip(1).First().Number, callbackJson["destinations"][1]["dnr"]);
			Assert.AreEqual(request.Contacts.Skip(1).First().Type, AlarmContactMessageTypeMapper.GetType(callbackJson["destinations"][1]["msgtype"].ToString()));

			Assert.AreEqual(request.Contacts.Skip(2).First().Name, callbackJson["destinations"][2]["name"]);
			Assert.AreEqual(request.Contacts.Skip(2).First().Number, callbackJson["destinations"][2]["dnr"]);
			Assert.AreEqual(request.Contacts.Skip(2).First().Type, AlarmContactMessageTypeMapper.GetType(callbackJson["destinations"][2]["msgtype"].ToString()));

			Assert.AreEqual(request.Contacts.Skip(3).First().Name, callbackJson["destinations"][3]["name"]);
			Assert.AreEqual(request.Contacts.Skip(3).First().Number, callbackJson["destinations"][3]["dnr"]);
			Assert.AreEqual(request.Contacts.Skip(3).First().Type, AlarmContactMessageTypeMapper.GetType(callbackJson["destinations"][3]["msgtype"].ToString()));

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingParameters()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new CreateContactsAlarmResponse { IsSuccess = true, Alarm = new CreateContactsAlarmObject { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new CreateContactsAlarmRequest();

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateContactsAlarm(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());

			try
			{
				var response = await api.CreateContactsAlarm(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string apikey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.NotFound;

			var request = new CreateContactsAlarmRequest
			{
				Message = "Achtung Probealarm",
				Contacts = new List<Contact>
				{
					new Contact
					{
						Name = "Kontakt 1",
						Number = "+49171000001",
						Type = AlarmContactMessageType.Voice
					}
				}
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateContactsAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual($"HTTP Request error: {httpResponseMessage.StatusCode}", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnErrorResponse()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateContactsAlarmResponse { HasError = true, ErrorMessage = "You shall not pass" };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateContactsAlarmRequest
			{
				Message = "Achtung Probealarm",
				Contacts = new List<Contact>
				{
					new Contact
					{
						Name = "Kontakt 1",
						Number = "+49171000001",
						Type = AlarmContactMessageType.Voice
					}
				}
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateContactsAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual(createResponse.ErrorMessage, ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse1()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateContactsAlarmResponse { IsSuccess = false, Alarm = new CreateContactsAlarmObject { IsSuccess = true } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateContactsAlarmRequest
			{
				Message = "Achtung Probealarm",
				Contacts = new List<Contact>
				{
					new Contact
					{
						Name = "Kontakt 1",
						Number = "+49171000001",
						Type = AlarmContactMessageType.Voice
					}
				}
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateContactsAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse2()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new CreateContactsAlarmResponse { IsSuccess = true, Alarm = new CreateContactsAlarmObject { IsSuccess = false } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new CreateContactsAlarmRequest
			{
				Message = "Achtung Probealarm",
				Contacts = new List<Contact>
				{
					new Contact
					{
						Name = "Kontakt 1",
						Number = "+49171000001",
						Type = AlarmContactMessageType.Voice
					}
				}
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				var response = await api.CreateContactsAlarm(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
