﻿using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public abstract class TestBase
	{
		internal Mock<HttpMessageHandler> httpMessageHandlerMock;
		internal HttpClient httpClient;

		internal string requestCallback;
		internal HttpResponseMessage httpResponseMessage;

		[TestInitialize]
		public virtual void InitializeTest()
		{
			requestCallback = null;
			httpResponseMessage = new HttpResponseMessage
			{
				StatusCode = HttpStatusCode.OK
			};
		}

		[TestCleanup]
		public virtual void CleanupTest()
		{
			httpClient?.Dispose();
		}

		internal PowerAlarmApi GetApi(string apikey)
		{
			httpMessageHandlerMock = new Mock<HttpMessageHandler>();
			httpMessageHandlerMock.Protected()
				.Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
				.Callback<HttpRequestMessage, CancellationToken>(async (req, _) =>
				{
					if (req.Content != null)
						requestCallback = await req.Content.ReadAsStringAsync();
				})
				.ReturnsAsync(httpResponseMessage);

			httpClient = new HttpClient(httpMessageHandlerMock.Object, disposeHandler: false);
			var api = new PowerAlarmApi
			{
				ApiKey = apikey
			};

			var field = api.GetType().GetField("httpClient", BindingFlags.NonPublic | BindingFlags.Instance);
			field.SetValue(api, httpClient);

			return api;
		}
	}
}
