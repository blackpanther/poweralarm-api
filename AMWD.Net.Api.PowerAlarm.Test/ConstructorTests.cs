﻿using System;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class ConstructorTests : TestBase
	{
		[TestMethod]
		public void ShouldCreateDefault()
		{
			// arrange

			// act
			using var api = new PowerAlarmApi();

			// assert
			Assert.IsNotNull(api);
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnDisposed()
		{
			// arrange
			var api = new PowerAlarmApi();

			// act
			api.Dispose();

			// assert
			try
			{
				await api.ReadGroups();
				Assert.Fail();
			}
			catch (ObjectDisposedException)
			{ /* expected behaviour */ }
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingApikey()
		{
			// arrange
			using var api = new PowerAlarmApi();

			// act + assert
			try
			{
				await api.ReadGroups();
				Assert.Fail();
			}
			catch (ApiKeyException)
			{ /* expected behaviour */ }
		}
	}
}
