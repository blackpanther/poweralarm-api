using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using AMWD.Net.Api.PowerAlarm.Requests;
using AMWD.Net.Api.PowerAlarm.Responses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class SetVehicleStatusTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFms()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				FmsId = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act
			bool response = await api.SetVehicleStatus(request);
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("setfms", callbackJson["action"]);
			Assert.AreEqual(request.FmsId, callbackJson["kennung"]);
			Assert.AreEqual(request.Status, callbackJson["status"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldMapOpta()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				OptaId = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act
			bool response = await api.SetVehicleStatus(request);
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("setfms", callbackJson["action"]);
			Assert.AreEqual(request.OptaId, callbackJson["opta"]);
			Assert.AreEqual(request.Status, callbackJson["status"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldMapIssi()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act
			bool response = await api.SetVehicleStatus(request);
			var callbackJson = JsonConvert.DeserializeObject<JObject>(requestCallback);

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.IsNotNull(response);

			Assert.AreEqual(apikey, callbackJson["apikey"]);
			Assert.AreEqual("setfms", callbackJson["action"]);
			Assert.AreEqual(request.Issi, callbackJson["issi"]);
			Assert.AreEqual(request.Status, callbackJson["status"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingParameters()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest();

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());

			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnNonExclusiveFms()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				FmsId = "AB12CD34",
				OptaId = "Fail",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnNonExclusiveOpta()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				OptaId = "AB12CD34",
				Issi = "Fail",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnNonExclusiveIssi()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				FmsId = "Fail",
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnWrongStatus()
		{
			// arrange
			string apikey = "abc";
			string responseJson = JsonConvert.SerializeObject(new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = true } });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");
			var request = new SetVehicleStatusRequest
			{
				FmsId = "AB12CD34"
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				request.Status = -5;
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException)
			{ /* expected behaviour */ }

			try
			{
				request.Status = 12;
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (ArgumentOutOfRangeException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string apikey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.NotFound;

			var request = new SetVehicleStatusRequest
			{
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual($"HTTP Request error: {httpResponseMessage.StatusCode}", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnErrorResponse()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new SetVehicleStatusResponse { HasError = true, ErrorMessage = "You shall not pass" };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new SetVehicleStatusRequest
			{
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual(createResponse.ErrorMessage, ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse1()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new SetVehicleStatusResponse { IsSuccess = false, Status = new VehicleStatus { IsSuccess = true } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new SetVehicleStatusRequest
			{
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnUnsuccessfulResponse2()
		{
			// arrange
			string apikey = "abc";
			var createResponse = new SetVehicleStatusResponse { IsSuccess = true, Status = new VehicleStatus { IsSuccess = false } };
			string responseJson = JsonConvert.SerializeObject(createResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, "application/json");

			var request = new SetVehicleStatusRequest
			{
				Issi = "AB12CD34",
				Status = 6
			};

			using var api = GetApi(apikey);

			// act + assert
			try
			{
				bool response = await api.SetVehicleStatus(request);
				Assert.Fail();
			}
			catch (PowerAlarmException ex)
			{
				Assert.AreEqual("Unknown error on PowerAlarm", ex.Message);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
