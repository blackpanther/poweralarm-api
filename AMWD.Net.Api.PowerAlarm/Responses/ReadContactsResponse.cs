﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Models;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm with contacts.
	/// </summary>
	public class ReadContactsResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets the alarm contacts.
		/// </summary>
		[JsonProperty("contacts")]
		public List<Contact> Contacts { get; set; } = new();
	}
}
