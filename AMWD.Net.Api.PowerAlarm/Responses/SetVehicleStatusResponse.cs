﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm to set a vehicle status.
	/// </summary>
	public class SetVehicleStatusResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets a value indicating whether the request was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Gets or sets the status object.
		/// </summary>
		[JsonProperty("fms")]
		public VehicleStatus Status { get; set; }
	}

	/// <summary>
	/// The vehicle status response.
	/// </summary>
	public class VehicleStatus
	{
		/// <summary>
		/// Gets or sets a value indicating whether the request was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Gets or sets the FMS identifier.
		/// </summary>
		[JsonProperty("kennung")]
		public string FmsId { get; set; }

		/// <summary>
		/// Gets or sets the OPTA identifier.
		/// </summary>
		[JsonProperty("opta")]
		public string OptaId { get; set; }

		/// <summary>
		/// Gets or sets the ISSI (Tetra).
		/// </summary>
		[JsonProperty("issi")]
		public string Issi { get; set; }

		/// <summary>
		/// Gets or sets the status as text.
		/// </summary>
		[JsonProperty("status")]
		public string StatusText { get; set; }

		/// <summary>
		/// Gets the parsed status.
		/// </summary>
		[JsonIgnore]
		public int Status => int.Parse(StatusText);

		/// <summary>
		/// Gets or sets the direction as text.
		/// </summary>
		[JsonProperty("richtung")]
		public string DirectionText { get; set; }

		/// <summary>
		/// Gets or sets the parsed direction.
		/// </summary>
		[JsonIgnore]
		public int Direction => int.Parse(DirectionText);
	}
}
