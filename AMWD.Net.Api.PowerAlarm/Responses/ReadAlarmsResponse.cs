﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Models;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm with contacts.
	/// </summary>
	public class ReadAlarmsResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets the alarm contacts.
		/// </summary>
		[JsonProperty("events")]
		public List<Alarm> Alarms { get; set; } = new();
	}
}
