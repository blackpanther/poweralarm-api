﻿using AMWD.Net.Api.PowerAlarm.Models;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm with status.
	/// </summary>
	public class CreateGroupAlarmResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets a value indicating whether the request was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Gets or sets the group status.
		/// </summary>
		[JsonProperty("triggergroupalarm")]
		public GroupAlarmStatus Alarm { get; set; }
	}
}
