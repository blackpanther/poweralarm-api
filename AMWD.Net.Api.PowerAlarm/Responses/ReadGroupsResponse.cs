﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Models;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm with groups.
	/// </summary>
	public class ReadGroupsResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets the alarm groups.
		/// </summary>
		[JsonProperty("groups")]
		public List<Group> Groups { get; set; } = new();
	}
}
