﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Models;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The response from PowerAlarm with states.
	/// </summary>
	public class CreateContactsAlarmResponse : BaseResponse
	{
		/// <summary>
		/// Gets or sets a value indicating whether the request was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// The triggered alarm.
		/// </summary>
		[JsonProperty("triggeralarm")]
		public CreateContactsAlarmObject Alarm { get; set; }
	}

	/// <summary>
	/// The contacts alarm response.
	/// </summary>
	public class CreateContactsAlarmObject
	{
		/// <summary>
		/// Gets or sets a value indicating whether the request was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// The alarm message.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }

		/// <summary>
		/// The status report per contact.
		/// </summary>
		[JsonProperty("destinations")]
		public List<ContactAlarmStatus> Contacts { get; set; } = new();
	}
}
