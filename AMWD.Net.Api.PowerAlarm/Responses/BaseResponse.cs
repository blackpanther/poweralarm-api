﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Responses
{
	/// <summary>
	/// The base for all responses.
	/// </summary>
	public abstract class BaseResponse
	{
		/// <summary>
		/// Gets or sets a value indicating whether an error occurred.
		/// </summary>
		[JsonProperty("error")]
		public bool HasError { get; set; }

		/// <summary>
		/// Get or sets the error message.
		/// </summary>
		[JsonProperty("errortext")]
		public string ErrorMessage { get; set; }
	}
}
