﻿using AMWD.Net.Api.PowerAlarm.Enums;

namespace AMWD.Net.Api.PowerAlarm.Utils
{
	/// <summary>
	/// Mapper for the <see cref="AlarmContactMessageType"/> enumeration.
	/// </summary>
	public static class AlarmContactMessageTypeMapper
	{
		/// <summary>
		/// Maps the type into the string representation.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public static string GetString(AlarmContactMessageType type)
		{
			return type switch
			{
				AlarmContactMessageType.Voice => "voice",
				AlarmContactMessageType.Text => "text",
				AlarmContactMessageType.Flash => "flash",
				AlarmContactMessageType.Fax => "fax",
				_ => null
			};
		}

		/// <summary>
		/// Maps the string representation into the type.
		/// </summary>
		/// <param name="type">The string representation.</param>
		/// <returns></returns>
		public static AlarmContactMessageType GetType(string type)
		{
			return type?.Trim().ToLower() switch
			{
				"voice" => AlarmContactMessageType.Voice,
				"text" => AlarmContactMessageType.Text,
				"flash" => AlarmContactMessageType.Flash,
				"fax" => AlarmContactMessageType.Fax,
				_ => AlarmContactMessageType.Undefined
			};
		}
	}
}
