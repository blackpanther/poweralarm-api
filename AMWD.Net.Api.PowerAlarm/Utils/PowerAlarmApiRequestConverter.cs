﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using AMWD.Net.Api.PowerAlarm.Requests;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm.Utils
{
	internal static class PowerAlarmApiRequestConverter
	{
		private static readonly string jsonMimeType = "application/json";

		#region Create group alarm

		public static HttpContent ConvertToHttpContent(this CreateGroupAlarmRequest request, string apikey)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			var contentObject = request.ConvertToJObject(apikey);
			return new StringContent(JsonConvert.SerializeObject(contentObject), Encoding.UTF8, jsonMimeType);
		}

		private static JObject ConvertToJObject(this CreateGroupAlarmRequest request, string apikey)
		{
			if (string.IsNullOrWhiteSpace(request.GroupId))
				throw new ArgumentNullException(nameof(request.GroupId), "The group is required");

			var jObj = new JObject
			{
				["apikey"] = apikey,
				["action"] = "triggergroupalarm",
				["kuerzel"] = request.GroupId.Trim(),
				["text"] = request.Message?.Trim() ?? ""
			};

			if (request.Latitude.HasValue)
				jObj["lat"] = request.Latitude.Value;

			if (request.Longitude.HasValue)
				jObj["long"] = request.Longitude.Value;

			if (!string.IsNullOrWhiteSpace(request.Country))
				jObj["land"] = request.Country.Trim();

			if (!string.IsNullOrWhiteSpace(request.Street))
				jObj["strasse"] = request.Street.Trim();

			if (!string.IsNullOrWhiteSpace(request.HouseNo))
				jObj["hsnr"] = request.HouseNo.Trim();

			if (!string.IsNullOrWhiteSpace(request.ZipCode))
				jObj["plz"] = request.ZipCode.Trim();

			if (!string.IsNullOrWhiteSpace(request.City))
				jObj["ort"] = request.City.Trim();

			if (!string.IsNullOrWhiteSpace(request.AdditionalInfo))
				jObj["zusatz"] = request.AdditionalInfo.Trim();

			if (!string.IsNullOrWhiteSpace(request.SceneOject))
				jObj["objekt"] = request.SceneOject.Trim();

			if (!string.IsNullOrWhiteSpace(request.Priority))
				jObj["prioritaet"] = request.Priority.Trim();

			if (!string.IsNullOrWhiteSpace(request.Units))
				jObj["einsatzmittel"] = request.Units.Trim();

			if (!string.IsNullOrWhiteSpace(request.Tetra))
				jObj["tetra"] = request.Tetra.Trim();

			if (!string.IsNullOrWhiteSpace(request.Fme))
				jObj["schleifen"] = request.Fme.Trim();

			return jObj;
		}

		#endregion Create group alarm

		#region Create conacts alarm

		public static HttpContent ConvertToHttpContent(this CreateContactsAlarmRequest request, string apikey)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			var contentObject = request.ConvertToJObject(apikey);
			return new StringContent(JsonConvert.SerializeObject(contentObject), Encoding.UTF8, jsonMimeType);
		}

		private static JObject ConvertToJObject(this CreateContactsAlarmRequest request, string apikey)
		{
			if (request.Contacts?.Any() != true)
				throw new ArgumentNullException(nameof(request.Contacts), "At least one contact is required");

			var jObj = new JObject
			{
				["apikey"] = apikey,
				["action"] = "triggeralarm",
				["text"] = request.Message?.Trim() ?? ""
			};

			var contacts = new JArray();
			jObj["destinations"] = contacts;

			foreach (var contact in request.Contacts)
			{
				var jContact = new JObject
				{
					["dnr"] = contact.Number,
					["msgtype"] = AlarmContactMessageTypeMapper.GetString(contact.Type)
				};

				if (!string.IsNullOrWhiteSpace(contact.Name))
					jContact["name"] = contact.Name.Trim();

				contacts.Add(jContact);
			}

			if (request.IsFeedbackAllowed.HasValue)
				jObj["feedback"] = request.IsFeedbackAllowed.Value;

			if (request.IsInfoMessage.HasValue)
				jObj["infomessage"] = request.IsInfoMessage.Value;

			return jObj;
		}

		#endregion Create conacts alarm

		#region Set vehicle status

		public static HttpContent ConvertToHttpContent(this SetVehicleStatusRequest request, string apikey)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			var contentObject = request.ConvertToJObject(apikey);
			return new StringContent(JsonConvert.SerializeObject(contentObject), Encoding.UTF8, jsonMimeType);
		}

		private static JObject ConvertToJObject(this SetVehicleStatusRequest request, string apikey)
		{
			if (string.IsNullOrWhiteSpace(request.FmsId) && string.IsNullOrWhiteSpace(request.OptaId) && string.IsNullOrWhiteSpace(request.Issi))
				throw new ArgumentNullException($"One of the identifier ({nameof(request.FmsId)}, {nameof(request.OptaId)}, {nameof(request.Issi)}) is required");

			if ((!string.IsNullOrWhiteSpace(request.FmsId) && (!string.IsNullOrWhiteSpace(request.OptaId) || !string.IsNullOrWhiteSpace(request.Issi))) ||
				(!string.IsNullOrWhiteSpace(request.OptaId) && (!string.IsNullOrWhiteSpace(request.FmsId) || !string.IsNullOrWhiteSpace(request.Issi))) ||
				(!string.IsNullOrWhiteSpace(request.Issi) && (!string.IsNullOrWhiteSpace(request.FmsId) || !string.IsNullOrWhiteSpace(request.OptaId))))
				throw new ArgumentException($"Only ONE identifier ({nameof(request.FmsId)}, {nameof(request.OptaId)}, {nameof(request.Issi)}) is allowed");

			if (request.Status < 0 || 9 < request.Status)
				throw new ArgumentOutOfRangeException(nameof(request.Status), "The status allowed range: 0-9");

			var jObj = new JObject
			{
				["apikey"] = apikey,
				["action"] = "setfms",
				["status"] = request.Status
			};

			if (!string.IsNullOrWhiteSpace(request.FmsId))
				jObj["kennung"] = request.FmsId.Trim();

			if (!string.IsNullOrWhiteSpace(request.OptaId))
				jObj["opta"] = request.OptaId.Trim();

			if (!string.IsNullOrWhiteSpace(request.Issi))
				jObj["issi"] = request.Issi.Trim();

			return jObj;
		}

		#endregion Set vehicle status
	}
}
