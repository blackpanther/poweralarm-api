﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.PowerAlarm.Exceptions;
using AMWD.Net.Api.PowerAlarm.Models;
using AMWD.Net.Api.PowerAlarm.Requests;
using AMWD.Net.Api.PowerAlarm.Responses;
using AMWD.Net.Api.PowerAlarm.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.PowerAlarm
{
	/// <summary>
	/// Implements the PowerAlarm API. (poweralarm.de)
	/// </summary>
	/// <seealso cref="IDisposable"/>
	public class PowerAlarmApi : IDisposable
	{
		/// <summary>
		/// The url to reach the API
		/// </summary>
		public const string API_URL = "https://www.poweralarm.de/api/custom/";
		private static readonly string jsonMimeType = "application/json";

		private readonly HttpClient httpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="PowerAlarmApi"/> class.
		/// </summary>
		public PowerAlarmApi()
		{
			httpClient = new HttpClient();
		}

		/// <summary>
		/// Gets or sets the api key.
		/// </summary>
		public string ApiKey { get; set; }

		/// <summary>
		/// Creates a new group alarm.
		/// [Alarm für eine Gruppe auslösen]
		/// </summary>
		/// <param name="request">The group alarm request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>A list with the group status.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required information are missing.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<GroupAlarmStatus> CreateGroupAlarm(CreateGroupAlarmRequest request, CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent(ApiKey);
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<CreateGroupAlarmResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			if (!response.IsSuccess || !response.Alarm.IsSuccess)
				throw new PowerAlarmException("Unknown error on PowerAlarm");

			return response.Alarm;
		}

		/// <summary>
		/// Creates a new contact alarm.
		/// [Alarm für übergebene Kontakte auslösen]
		/// </summary>
		/// <param name="request">The contact alarm request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>A list with the contact status.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required information are missing.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<List<ContactAlarmStatus>> CreateContactsAlarm(CreateContactsAlarmRequest request, CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent(ApiKey);
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<CreateContactsAlarmResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			if (!response.IsSuccess || !response.Alarm.IsSuccess)
				throw new PowerAlarmException("Unknown error on PowerAlarm");

			return response.Alarm.Contacts;
		}

		/// <summary>
		/// Reads the groups.
		/// [Gruppen einlesen]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>A list with the groups.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<List<Group>> ReadGroups(CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();
			var requestContent = GetReadRequest("getgroups");
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<ReadGroupsResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			return response.Groups;
		}

		/// <summary>
		/// Reads the contacts.
		/// [Kontakte einlesen]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>A list with the contacts.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<List<Contact>> ReadContacts(CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();
			var requestContent = GetReadRequest("getcontacts");
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<ReadContactsResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			return response.Contacts;
		}

		/// <summary>
		/// Reads the alarms.
		/// [Alarme einlesen]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>A list with the alarms.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<List<Alarm>> ReadAlarms(CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();
			var requestContent = GetReadRequest("getalarms");
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<ReadAlarmsResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			return response.Alarms;
		}

		/// <summary>
		/// Sets a vehicle status.
		/// [Fahrzeugstatus setzen]
		/// </summary>
		/// <param name="request">The vehicle status request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="ApiKeyException">The api key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required information are missing.</exception>
		/// <exception cref="ArgumentException">The request information are invalid.</exception>
		/// <exception cref="ArgumentOutOfRangeException">The provided status is invalid.</exception>
		/// <exception cref="PowerAlarmException">An PowerAlarm error occurrs.</exception>
		public async Task<bool> SetVehicleStatus(SetVehicleStatusRequest request, CancellationToken cancellationToken = default)
		{
			CheckApiKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent(ApiKey);
			var httpResponse = await httpClient.PostAsync(API_URL, requestContent, cancellationToken);
			if (!httpResponse.IsSuccessStatusCode)
				throw new PowerAlarmException($"HTTP Request error: {httpResponse.StatusCode}");

			string responseContent = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<SetVehicleStatusResponse>(responseContent);
			if (response.HasError)
				throw new PowerAlarmException(response.ErrorMessage);

			if (!response.IsSuccess || !response.Status.IsSuccess)
				throw new PowerAlarmException("Unknown error on PowerAlarm");

			return true;
		}

		private HttpContent GetReadRequest(string action)
		{
			return new StringContent(JsonConvert.SerializeObject(new JObject
			{
				["apikey"] = ApiKey,
				["action"] = action
			}), Encoding.UTF8, jsonMimeType);
		}

		#region IDisposable implementation

		private bool isDisposed;

		/// <inheritdoc/>
		public void Dispose()
		{
			if (isDisposed)
				return;

			isDisposed = true;
			httpClient.Dispose();
		}

		private void CheckApiKeyOrDisposed()
		{
			if (isDisposed)
				throw new ObjectDisposedException(GetType().FullName);

			if (string.IsNullOrWhiteSpace(ApiKey))
				throw new ApiKeyException("The API key is required");
		}

		#endregion IDisposable implementation
	}
}
