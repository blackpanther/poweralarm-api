﻿namespace AMWD.Net.Api.PowerAlarm.Requests
{
	/// <summary>
	/// The request model to set a vehicle status.
	/// </summary>
	public class SetVehicleStatusRequest
	{
		/// <summary>
		/// Gets or sets a FMS identifier.
		/// </summary>
		public string FmsId { get; set; }

		/// <summary>
		/// Gets or sets an OPTA identifier.
		/// </summary>
		public string OptaId { get; set; }

		/// <summary>
		/// Gets or sets an ISSI (Tetra).
		/// </summary>
		public string Issi { get; set; }

		/// <summary>
		/// Gets or sets the status [0-9].
		/// </summary>
		public int Status { get; set; }
	}
}
