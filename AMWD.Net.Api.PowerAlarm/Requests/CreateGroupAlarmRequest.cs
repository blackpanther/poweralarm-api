﻿namespace AMWD.Net.Api.PowerAlarm.Requests
{
	/// <summary>
	/// The request model to create a new alarm.
	/// </summary>
	public class CreateGroupAlarmRequest
	{
		/// <summary>
		/// Gets or sets the group name.
		/// [Gruppenkürzel]
		/// </summary>
		public string GroupId { get; set; }

		/// <summary>
		/// Gets or sets the alarm text.
		/// [Alarmierungstext]
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the latitude.
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the longitude.
		/// </summary>
		public float? Longitude { get; set; }

		/// <summary>
		/// Gets or sets the street name.
		/// </summary>
		public string Street { get; set; }

		/// <summary>
		/// Gets or sets the house number.
		/// </summary>
		public string HouseNo { get; set; }

		/// <summary>
		/// Gets or sets the city's zip code.
		/// </summary>
		public string ZipCode { get; set; }

		/// <summary>
		/// Gets or sets the city.
		/// </summary>
		public string City { get; set; }

		/// <summary>
		/// Gets or sets the country.
		/// [D / A / L / CH / F]
		/// </summary>
		public string Country { get; set; }

		/// <summary>
		/// Gets or sets additional information.
		/// [Zusatzinfos, z.B. 2. OG]
		/// </summary>
		public string AdditionalInfo { get; set; }

		/// <summary>
		/// Gets or sets information of the scene object.
		/// </summary>
		public string SceneOject { get; set; }

		/// <summary>
		/// Gets or sets the priority.
		/// </summary>
		public string Priority { get; set; }

		/// <summary>
		/// Gets or sets the units.
		/// </summary>
		public string Units { get; set; }

		/// <summary>
		/// Gets or sets the tetra issi.
		/// </summary>
		public string Tetra { get; set; }

		/// <summary>
		/// Gets or sets the fme number.
		/// </summary>
		public string Fme { get; set; }
	}
}
