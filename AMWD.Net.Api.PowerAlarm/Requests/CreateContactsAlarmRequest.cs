﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Models;

namespace AMWD.Net.Api.PowerAlarm.Requests
{
	/// <summary>
	/// The request modal to create a new alarm.
	/// </summary>
	public class CreateContactsAlarmRequest
	{
		/// <summary>
		/// Gets or sets the alarm message.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the list of contacts to notify.
		/// </summary>
		public List<Contact> Contacts { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether feedback is allowed.
		/// </summary>
		public bool? IsFeedbackAllowed { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is an informational message.
		/// </summary>
		public bool? IsInfoMessage { get; set; }
	}
}
