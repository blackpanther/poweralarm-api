﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Models
{
	/// <summary>
	/// The alarm status of a group.
	/// </summary>
	public class GroupAlarmStatus
	{
		/// <summary>
		/// Gets or sets a value indicating whether the alarm was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Gets or sets the group.
		/// </summary>
		[JsonProperty("kuerzel")]
		public string GroupId { get; set; }

		/// <summary>
		/// Gets or sets the alarm message.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets event ids.
		/// </summary>
		[JsonProperty("eventid")]
		public List<string> EventIds { get; set; } = new();
	}
}
