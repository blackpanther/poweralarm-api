﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Models
{
	/// <summary>
	/// A alarm group.
	/// </summary>
	public class Group
	{
		/// <summary>
		/// Gets or sets the group.
		/// </summary>
		[JsonProperty("kuerzel")]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }
	}
}
