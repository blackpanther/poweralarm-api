﻿using System.Collections.Generic;
using AMWD.Net.Api.PowerAlarm.Enums;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Models
{
	/// <summary>
	/// A alarm contact.
	/// </summary>
	public class Contact
	{
		/// <summary>
		/// Gets or sets a name.
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets a short name.
		/// </summary>
		[JsonProperty("kuerzel")]
		public string ShortName { get; set; }

		/// <summary>
		/// Gets or sets additional information.
		/// </summary>
		[JsonProperty("zusatz")]
		public string AdditionalInformation { get; set; }

		/// <summary>
		/// Gets or sets a (mobile) number.
		/// </summary>
		[JsonProperty("phone")]
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets the message type.
		/// </summary>
		public AlarmContactMessageType Type { get; set; }

		/// <summary>
		/// Gets or sets a list of mapped groups.
		/// </summary>
		[JsonProperty("groups")]
		public List<string> GroupIds { get; set; }

		/// <summary>
		/// Gets or sets a list of mapped sub-groups.
		/// </summary>
		[JsonProperty("subgroups")]
		public List<string> SubGroupIds { get; set; }
	}
}
