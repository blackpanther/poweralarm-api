﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Models
{
	/// <summary>
	/// An alarm.
	/// </summary>
	public class Alarm
	{
		/// <summary>
		/// Gets or sets the date as 'yyyy-MM-dd HH:mm:ss'.
		/// </summary>
		[JsonProperty("date")]
		public string Date { get; set; }

		/// <summary>
		/// Gets the timestamp (from <see cref="Date"/>).
		/// </summary>
		[JsonIgnore]
		public DateTime Timestamp => DateTime.ParseExact(Date, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

		/// <summary>
		/// Gets or sets the group.
		/// </summary>
		[JsonProperty("group")]
		public string GroupId { get; set; }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		[JsonProperty("text")]
		public string Message { get; set; }
	}
}
