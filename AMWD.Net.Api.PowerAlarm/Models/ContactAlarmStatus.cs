﻿using AMWD.Net.Api.PowerAlarm.Enums;
using AMWD.Net.Api.PowerAlarm.Utils;
using Newtonsoft.Json;

namespace AMWD.Net.Api.PowerAlarm.Models
{
	/// <summary>
	/// The alarm status of a contact.
	/// </summary>
	public class ContactAlarmStatus
	{
		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		[JsonProperty("dnr")]
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the message type (PowerAlarm).
		/// </summary>
		[JsonProperty("msgtype")]
		public string MsgType { get; set; }

		/// <summary>
		/// Gets the <see cref="MsgType"/> as mapped type.
		/// </summary>
		[JsonIgnore]
		public AlarmContactMessageType Type => AlarmContactMessageTypeMapper.GetType(MsgType);

		/// <summary>
		/// Gets or sets a value indicating whether the alarm was successful.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }
	}
}
