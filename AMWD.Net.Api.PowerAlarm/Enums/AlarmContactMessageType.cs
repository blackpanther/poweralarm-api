﻿namespace AMWD.Net.Api.PowerAlarm.Enums
{
	/// <summary>
	/// Listing of message types.
	/// </summary>
	public enum AlarmContactMessageType
	{
		/// <summary>
		/// Undefined, specified by group.
		/// </summary>
		Undefined = 0,
		/// <summary>
		/// Voice call.
		/// </summary>
		Voice = 1,
		/// <summary>
		/// Text message.
		/// </summary>
		Text = 2,
		/// <summary>
		/// Flash message.
		/// </summary>
		Flash = 3,
		/// <summary>
		/// Fax message.
		/// </summary>
		Fax = 4
	}
}
